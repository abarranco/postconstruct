package com.autentia.beans;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Bean3 {

	private final Bean2 bean2;
	
	@Autowired
	public Bean3 (Bean2 bean2) {
		
		System.out.println("Bean 3 Constructor called with dependency injection");
		this.bean2 = bean2;
	}
	
	@PostConstruct
	private final void init() {
		System.out.println("Bean 3 @PostConstructor called");
	}
}