package com.autentia.beans;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Bean5 {
	
	@Autowired
	public Bean5() {
		
		System.out.println("Bean 5 Constructor called");
	}
	
	@PostConstruct
	private final void init() {
		System.out.println("Bean 5 @PostConstructor called");
	}
}