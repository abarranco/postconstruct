package com.autentia.beans;
import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Bean1 {

	private final Bean2 bean2;
	
	private final Bean3 bean3;
	
	@Autowired
	public Bean1 (Bean2 bean2, Bean3 bean3) {
		
		System.out.println("Bean 1 Constructor called with dependency injection");
		this.bean2 = bean2;
		this.bean3 = bean3;
	}
	
	@PostConstruct
	private final void init() {
		System.out.println("Bean 1 @PostConstructor called");
	}
}