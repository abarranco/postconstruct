package com.autentia.beans;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Bean4 {
	
	private final Bean5 bean5;

	@Autowired
	public Bean4 (Bean5 bean5) {
		
		System.out.println("Bean 4 Constructor called with dependency injection");
		this.bean5 = bean5;
	}
	
	@PostConstruct
	private final void init() {
		System.out.println("Bean 4 @PostConstructor called");
	}
}