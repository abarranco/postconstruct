package com.autentia.beans;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Bean2 {
	
	private final Bean4 bean4;

	@Autowired
	public Bean2 (Bean4 bean4) {
		
		System.out.println("Bean 2 Constructor called with dependency injection");
		this.bean4 = bean4;
	}
	
	@PostConstruct
	private final void init() {
		System.out.println("Bean 2 @PostConstructor called");
	}
}